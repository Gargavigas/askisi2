package jsoupparser;


import jsoupparser.*;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.Scanner;


public class AnalisiHtml {
    public static void main(String[] args) throws IOException {
       System.out.println("Insert web site: ");
       Scanner scanner = new Scanner(System.in);
       String url = scanner.nextLine();
       System.out.println("Your website is " + url);

        Document doc = Jsoup.connect(url).get();
        Elements links = doc.select("a[href]");
        Elements js = doc.select("script[src$=.js]");
        Elements css = doc.select("link[rel=stylesheet][type=text/css][href$=.css]");

        print("\nLinks: (%d)", links.size());
        for (Element src : js) {
            System.out.println(src);
        }
        for (Element css1 : css) {
            System.out.println(css1);
        }
    }

    private static void print(String msg, Object... args) {
        System.out.println(String.format(msg, args));    
    }

    private static String trim(String s, int width) {
        if (s.length() > width)
            return s.substring(0, width-1) + ".";
        else
            return s;
    }
